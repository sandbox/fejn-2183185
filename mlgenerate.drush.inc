<?php

/**
 * Implementation of hook_drush_help(). Basic drush help
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'. This hook is optional. If a command
 * does not implement this hook, the command's description is used instead.
 *
 * This hook is also used to look up help metadata, such as help
 * category title and summary.  See the comments below for a description.
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function mlgenerate_drush_help($section) {
drush_print($section);
  switch ($section) {
    case 'drush:mlgenerate':
		  $s = 'main';
      $help = dt('Automates generating bs for your multinational sites. ');
      $help .= dt('If no language is specified, defaults to none => Lorem ipsum.');
      $help .= dt('If no character set is specified, defaults to ASCII -> ISO-8859-1.');
			$help .= dt('If no format specified, generates paragraph formatted content.');
      break;
    case 'drush:mlgenerate-language':
		  $s = 'language';		
      $help = dt('List of languages that content can be generated for.');
			break;
    case 'drush:mlgenerate-encoding':
		  $s = 'encoding';		
      $help = dt('List of encodings (character sets) available');
			break;
    case 'drush:mlgenerate-items':
		  $s = 'items';
      $help = dt('List items(nodes, articles, etc.) content can be generated for.');
			break;
    case 'drush:mlgenerate-formats':
		  $s = 'format';	
      $help = dt('List available content formats(paragraph, etc.) that can be generated.');
			break;
    // The 'title' meta item is used to name a group of
    // commands in `drush help`.  If a title is not defined,
    // the default is "All commands in ___", with the
    // specific name of the commandfile (e.g. sandwich).
    // Command files with less than four commands will
    // be placed in the "Other commands" section, _unless_
    // they define a title.  It is therefore preferable
    // to not define a title unless the file defines a lot
    // of commands.
    case 'meta:mlgenerate:title':
		  $s = 'meta:title';
      $help = dt("ML commands");
			break;
    // The 'summary' meta item is displayed in `drush help --filter`,
    // and is used to give a general idea what the commands in this
    // command file do, and what they have in common.
    case 'meta:mlgenerate:summary':
		  $s = 'meta:summary';
      $help = dt("Automates generating bs for your multinational sites.");
			break;
			
			drush_print($help);
      return $help;		// return the help text.				
  }
}

/**
 * Implements hook_drush_command().
 */
 
function mlgenerate_drush_command() {
  $items = array();
	
  // Name of the main drush command
  $items['mlgenerate'] = array(
    'description' => dt('generate dummy m/l content.'),
    'callback' => 'drush_mlgenerate_mlgenerate',		
    'arguments'   => array(
      '-l <lang-string>'    => dt('Language content generated for.'),
      '-t <type-string>'    => dt('Type of content to generate.'),
      '-n <number>'    => dt('Number of content elements to generate.'),						
    ),
    'examples' => array(
      'Standard example' => 'drush mlgen',
      'Argument example' => 'drush mlgen -l "Chinese" -t "paragraph" -n 5',
    ),
    'aliases' => array('mlgen'),
  );

  // mlgenerate-list allows one to see details of parameters that can be specified with mlgenerate.	
	$items['mlgenerate-list'] = array(
	  'description' => dt('Detailed list of parameters that can be used with mlgenerate.'),
    'callback' => 'drush_mlgenerate_list',
    'arguments' => array(
      'arg1' => 'What you want information about: languages, encoding, type, format.',
    ),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
    'aliases' => array('mll'),
    'examples' => array(
      'drush mll languages' => 'List languages that content can be generated for.',
			'drush mll encoding' => 'List character sets(encoding) to use for content generated.',
			'drush mll types' => 'List types of content that can be generated.',
			'drush mll formats' => 'List content formats that can be generated.',						
    ),
  );
  return $items;
}

/**
 * Callback function for drush ml-generate. 
 * Callback is called by using drush_hook_command() where
 * hook is the name of the module (MYMODULE) and command is the name of
 * the Drush command with all "-" characters converted to "_" characters (my_command)
 *
 * @param $arg1
 *   An optional argument
 */
function drush_mlgenerate_mlgenerate($arg1 = NULL) {
  //This is currently only a stub command - flesh it out later
	
  //check if the argument was passed in and just print it out
  if (isset($arg1)) {
   drush_print($arg1);
  }
 
  //log to the command line with an OK status
  drush_log('Running mlgenerate', 'ok');
} 

/**
 * Callback function for drush mll (drush mlgenerate-list). 
 * Callback is called by using drush_hook_command() where
 * hook is the name of the module (MYMODULE) and command is the name of
 * the Drush command with all "-" characters converted to "_" characters (my_command)
 *
 * @param $arg1
 *   An required argument telling what you want info on.
 */
function drush_mlgenerate_list($arg1 = NULL) {
  //This is currently only a stub command - flesh it out with the arguments later

  //check if the argument was passed in and just print it out
  if (isset($arg1)) {
   drush_print($arg1);
  }	else {
	 drush_print('You must tell us what you want to know about.');
	}
 
  //log to the command line with an OK status
  drush_log('Running mlgenerate', 'ok');
} 
