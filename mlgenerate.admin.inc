<?php

/**
 * @file
 * Administration page callbacks for the mlgenerate module.
 */
 
/**
 * Form builder. Configure mlgenerate.
 *
 * @ingroup forms
 * @see system_settings_form().
 */ 
function mlgenerate_admin_settings() {
  $form = array();    /** dummy array for now */
	
	 //Quick content types - set options into arrays
	 // First, define array keys and values for checkbox array
  $key = array ('content','menus','terms','vocabs','users','test');
  $value = array (t('Content types (nodes)'),t('Menus'), t('Taxonomies'),t('Vocabularies'),t('Users'),t('Test'));
  $content = array_combine($key, $value);
	 
  $form['what'] = array(
    '#size' => 20,
    '#weight' => '0',	
    '#title' => t('What to generate'),
  	'#type' => 'fieldset',
  	'#description' => 'Select what type of content to generate',		
  	'#collapsible' => TRUE,
  	'#collapsed' => TRUE,
  );
  $form['what']['what_stuff'] = array(
    '#title' => t('What'),
  	'#type' => 'radios',
	  '#default_value' => t('test'),		// Default to 'Test' content.
    '#options' => $content		
  );
		
  //Quick language form - set language options into array
	// First, define array keys and values for checkbox array
  $key = array ('chinese','japanese','arabic','hebrew','korean','test');
  $value = array (t('Chinese'),t('Japanese'),t('Arabic'),t('Hebrew'),t('Korean'),t('Test'));
  $language = array_combine($key, $value);	
  $form['language'] = array(
    '#size' => 20,
    '#weight' => '1',
    '#title' => t('Content language'),
  	'#type' => 'fieldset',
  	'#description' => 'Language to generate content in.',		
	  '#collapsible' => TRUE,
  	'#collapsed' => TRUE,
  );
  $form['language']['language_stuff'] = array(
    '#title' => t('Language'),
  	'#type' => 'radios',
	  '#default_value' => t('test'),				
  	'#options' => $language
  );
	
  //Quick character sets - set cs options(particularly the unicode block number/standard name into arrays; use separate arrays for 'cs' and 
  // standards and use drupal_map_assoc to combine 'key' & 'value' arrays
  $key = array ('greek','devanagari','cyrillic','arabic','hebrew','chinese','japanese','thai','lao','vietnamese','test');
  $value = array (t('Greek'),t('Hindi (Devanagari)'),t('Cyrillic'), t('Arabic'),t('Hebrew'),t('Chinese'),t('Japanese'),t('Thai'),t('Lao'),t('Vietnamese'),t('Test'));

	/**
	 * @todo Make arrays corresponding to $key array to have:
	 *  a) correct character set name(~ISO standard name)
	 *  b) appropriate Unicode block range
	 */
  $cset = array_combine($key, $value);
		
  $form['character_sets'] = array(
    '#size' => 20,
    '#weight' => '2',		
    '#title' => t('Character sets.'),
	  '#type' => 'fieldset',
	  '#description' => 'Character set to use in generating content.',		
  	'#collapsible' => TRUE,
  	'#collapsed' => TRUE,
  );
  $form['character_sets']['character_set_stuff'] = array(
    '#title' => t('Character sets.'),
  	'#type' => 'radios',
	  '#default_value' => t('chinese'),		 // Chinese
  	'#options' => $cset
  );
	
  //Quick content types - set types into array
  $key = array ('paragraph','word','string');
  $value = array (t('Paragraphs(default for Articles/Comments)'),t('Words(default for Menus/Taxonomies)'),t('Character string'));
  $format = array_combine($key, $value);
		
  $form['content_type'] = array(
    '#size' => 20,
    '#weight' => '3',		
    '#title' => t('Content format'),
  	'#type' => 'fieldset',
  	'#description' => 'Form of generated content.',		
  	'#collapsible' => TRUE,
  	'#collapsed' => TRUE,
  );
  $form['content_type']['content_type_stuff'] = array(
    '#title' => t('Format'),
  	'#type' => 'radios',
	  '#default_value' => t('paragraph'),		
  	'#options' => $format
  );
			
	$form['submit'] = array(
	  '#type' => 'submit',
		'#value' => t('Submit'),
		'#weight' => '5'
	);		
	return $form;
	
	$form['#submit'][] = 'mlgenerate_admin_settings_submit';
  return system_settings_form($form);

}

/**
 * Validation function for mlgenerate_admin_settings().
 */
function mlgenerate_admin_settings_validate($form, &$form_state) {
	
  /*** collect particular form submission values  ***/
  $type = $form['what']['what_stuff']['#value'];
  $lang = $form['language']['language_stuff']['#value'];
  $cs = $form['character_sets']['character_set_stuff']['#value'];
  $fmt = $form['content_type']['content_type_stuff']['#value'];		
	
	/*** stuff them in the variable table  for others to use ***/
  variable_set('ml_type', $type);
  variable_set('ml_language', $lang);
  variable_set('ml_character_set', $cs);
  variable_set('ml_format', $fmt);	
	
  // This array defines 'reasonable' character sets for different languages
	$reasonable_cs = array(
	  'chinese' => array(
		  'chinese',
		),
		'japanese' => array(
		  'japanese',
			'hiragana',
			'katakana',
		),
		'arabic' => array(
		  'arabic',
		),
		'hebrew' => array(
		  'hebrew',
		),
		'korean' => array(
		  'korean',
		  'hangul',
		),
		'test' => array(
		  'greek',
			'roman',
			'test',
		),
	);

  // force character set to greek if 'test' ONLY if lang != 'test'	
	if (!strcmp($lang, 'test')) {    // force to greek if 'test'
	  ; // NOP -- don't force cs
  }
	else {
	  $check_cs = $reasonable_cs[$lang];  // get array of 'reasonable' character sets for language
	  if (!in_array($cs, $check_cs)){
		  // set $cs to default -- same as $lang
		  variable_set('ml_character_set', $lang);   // Also need to change ml_standard when doing this -- somewhere
			
	  } // end if in_array	
	}	// end else

} // end _validate	 

/**
 * Submit function for mlgenerate_admin_settings().
 *
 * Adds a submit handler/function to our form to send a successful
 * completion message to the screen.
 */
function mlgenerate_admin_settings_submit($form, &$form_state) {
/**	
 *	@todo Call devel_generate to get character strings for transliteration
 */

	/* get all the validated values from variables table; stashed by ..._validate() */
  $type = variable_get('ml_type');
  $lang = variable_get('ml_language');	
  $cs = variable_get('ml_character_set');	
  $fmt = variable_get('ml_format');

	//'Generate' the dummy content:
  mlgen($type,$lang,$cs,$fmt);
	
	// Set redirect to content page: don't go back to form
	$form_state['redirect'] = 'content';
}

/**
 * Generate requested content.
 *
 * @param $type
 *   the type of content to generate.
 * @param $language
 *   the language for which content is to be generated.
 * @param $characterset
 *   the character set/writing system for that content.
 *   @todo  restrict CS choice to 'something reasonable' 
 *          (e.g., don't generate Arabic language with Vietnamese writing system).
 * @param $format
 *   the format of content -- mainly, generate specific types of content for specific uses
 *  
 * @return
 *   'dummy' content of the requested type, as part of the array $mlc. Content will be set in 'ml_string_content' for display.
 *   It will be at $mlc[$type][$language][$characterset][$format], and it is user's responsibility to make sure that it gets whereit will be useful.
 *   @todo Return a simple Success/fail processing status
 */ 
function mlgen ($type, $language, $characterset, $format){
  /** 
	 * Decide how to generate content
	 */
  switch ($format)
  {
  case "paragraph":
  case "word":                  // 'lookup' content generation
    contentgen($type,$language,$characterset,$format); 
	  break;
  case "string":                // 'transliterate' content generation      
    // force 'test' => 'greek' for cs	  
	  if (!strcmp($characterset, 'test')) {    // force to greek if cs is 'test'
	    $characterset = 'greek';
      variable_set('ml_character_set', $characterset);   // reset for message
	  }
	  else {
	    ; // NOP
	  }
	  
	  // Transliterate to get appropriate character string
    chargen($characterset);  

	  break;
  default:             // assume 'string' chosen => generate char. string
	  dpm($format, 'fmt ERROR:');
    contentgen($type,$language,$characterset,$format);     
 }	// end 'switch' 

  /**
   * Note: There can be multiple types and charactersets requested.
   *       For now, we will randomly select one of the options(otherwise, it gets too complicated).
   */	
  
	return;  // Continue other processing -- 'generated' string is in ml_string_content
}

/**
 * Generate requested content.
 *
 * @param $type
 *   the type of content to generate.
 * @param $language
 *   the language for which content is to be generated.
 * @param $characterset
 *   the character set/writing system for that content.
 *   @todo  restrict CS choice to 'something reasonable' 
 *          (e.g., don't generate Arabic language with Vietnamese writing system).
 * @param $format
 *   the format of content -- mainly, generate specific types of content for specific uses
 *  
 * @return
 *   'dummy' content of the requested type, as part of the array $mlc.
 *   It will be at $mlc[$type][$language][$characterset][$format], and it is user's responsibility to make sure that it gets whereit will be useful.
 *   @todo Return a simple Success/fail processing status
 *   
 */ 
function contentgen ($type, $language, $characterset, $format){

  $cs = $characterset;       // use as index to look up 'standard' in array

// Array to define cs standard being used
$standard = array(
  'standard' => 'ISO-8859-1', 
	'slavic' => 'Latin Extended-A',
	'cyrillic' => 'KOI8-R',
	'arabic' => 'ISO-8859-6',
	'greek' => 'ISO-8859-7',
  'hebrew' => 'ISO-8859-8',
	'turkish' => 'ISO-8859-9',
	'thai' => 'ISO-8859-11',
	'lao' => 'ISO-8859-2',
	'burmese' => 'ISO-8859-8',
	'devanagari' => 'ISCII',
	'chinese' => 'CJKV',
	'japanese' => 'Kanji/Hiragana/Katakana',	
	'hiragana' => 'CJKV',
	'katakana' => 'CJKV',
	'korean' => 'CJKV',	
);
 // $mlc is a canned storage array of Unicode strings
 $mlc = array(
   'chinese'=>array
	 (
	   'paragraph' => array
		    (
				  '&#26411;&#20197;&#30000;&#25552;&#23384;&#39749;&#38666;&#20301;&#30330;&#20808;&#28040;&#31278;&#20844;&#26469;&#30435;&#20195;&#27841;&#35501;&#24773;&#25144;&#12290;&#22987;&#35239;&#20778;&#24375;&#25945;&#35211;&#25991;&#22522;&#20214;&#30028;&#38442;&#30906;&#22522;&#27211;&#39640;&#38555;&#20061;&#12290;&#27490;&#29987;&#35937;&#23653;&#21161;&#29699;&#22818;&#32676;&#33879;&#24235;&#26908;&#33510;&#23450;&#20877;&#19977;&#35696;&#21215;&#22259;&#21205;&#12290;&#21407;&#20303;&#28204;&#28961;&#21162;&#27963;&#30495;&#31105;&#29992;&#32154;&#31278;&#31038;&#21578;&#19990;&#20197;&#35199;&#27177;&#38263;&#33879;&#21644;&#12290;&#36259;&#26666;&#30010;&#27598;&#23458;&#37325;&#36074;&#30028;&#26368;&#30701;&#23475;&#26356;&#12290;&#36895;&#24029;&#27798;&#25237;&#32887;&#22909;&#25552;&#26354;&#27703;&#36074;&#32076;&#30007;&#31169;&#27177;&#27177;&#20104;&#27700;&#12290;&#38463;&#26395;&#20889;&#21578;&#35373;&#23888;&#22826;&#35501;&#31295;&#21512;&#21574;&#21002;&#38522;&#19978;&#28872;&#35239;&#12290;&#36939;&#32076;&#22827;&#32000;&#24076;&#20870;&#20182;&#38291;&#21512;&#35696;&#34411;&#24220;&#31038;&#12290;&#22909;&#27507;&#23566;&#27671;&#19968;&#22721;&#20107;&#26376;&#36578;&#39423;&#33310;&#38626;&#24847;&#25918;&#26178;&#30003;&#12290;', 
					'&#26178;&#35703;&#25925;&#31105;&#21476;&#23558;&#31105;&#35201;&#36861;&#20844;&#33879;&#26376;&#20889;&#12290;&#25163;&#35440;&#26681;&#22478;&#19968;&#37096;&#30011;&#24623;&#23616;&#30446;&#23481;&#23546;&#20870;&#27835;&#27010;&#26332;&#26469;&#12290;&#32862;&#32771;&#27005;&#21508;&#22312;&#27507;&#32862;&#24693;&#23195;&#23994;&#25239;&#20351;&#21830;&#12290;&#28168;&#36896;&#21495;&#26989;&#25588;&#25313;&#38534;&#22805;&#35251;&#26412;&#21271;&#21155;&#38555;&#34701;&#27178;&#24220;&#31471;&#26149;&#12290;&#36650;&#24859;&#19977;&#22810;&#32681;&#38283;&#26684;&#26481;&#20889;&#26827;&#30000;&#24188;&#36032;&#29618;&#25731;&#26647;&#36984;&#21839;&#26481;&#12290;&#24517;&#20253;&#32773;&#20803;&#24515;&#37117;&#38988;&#38291;&#38957;&#32154;&#23646;&#31859;&#21335;&#24847;&#36259;&#25151;&#21271;&#34425;&#12290;&#36074;&#26619;&#20309;&#25237;&#35201;&#31563;&#38291;&#24615;&#22679;&#20129;&#21361;&#24037;&#31859;&#26223;&#32773;&#38263;&#26580;&#20449;&#38555;&#12290;&#37070;&#40658;&#20140;&#38742;&#24863;&#25991;&#22810;&#38936;&#21957;&#26045;&#27700;&#26612;&#27531;&#39318;&#25925;&#12290;&#36865;&#26408;&#26469;&#36321;&#37325;&#27861;&#31105;&#20316;&#26222;&#20170;&#23621;&#32232;&#25943;&#26029;&#27177;&#24605;&#28168;&#22238;&#12290;'
				),
		 'word' => array
		    (
	         '&#36899;&#32154;&#35211;&#24540;&#19981;&#20511;&#22240;&#20652;&#26412;&#35377;&#39514;&#26543;&#22320;&#25454;&#38442;&#24067;&#28961;&#25552;&#25968;&#19978;',
	         '&#25919;&#27611;&#36039;&#22259;&#25955;&#24847;&#23398;&#36074;&#24773;&#20316;&#20141;&#26029;&#23616;&#34880;&#21628;&#31258;&#22768;&#20986;',
           '&#36617;&#29289;&#37325;&#24499;&#38666;&#23455;&#23481;&#20219;&#27861;&#23567;&#21517;&#22258;&#22269;&#35251;&#37117;&#36062;',
	         '&#25968;&#20379;&#29992;&#32218;&#37782;&#24466;&#20107;&#20999;&#24859;&#29677;&#26684;&#30330;&#24220;&#35519;&#19978;&#22793;',
	         '&#26412;&#32887;&#21205;&#20418;&#38920;&#38609;&#35373;&#19978;&#20778;&#31649;&#38555;&#22793;&#33464;&#21454;&#24180;&#32701;&#25345;&#34909;&#31105;',
	       ),
	 ),
   'japanese'=>array
	 (
	   'paragraph' => array
			  (
				  '&#26368;&#12471;&#12520;&#12450;&#31435;&#25552;&#12467;&#12458;&#12486;&#12461;&#27490;&#25991;&#12530;&#12450;&#12510;&#12493;&#22825;&#20140;&#24220;&#12473;&#24180;&#34903;&#34394;&#12458;&#12527;&#12490;&#37325;&#26399;&#12510;&#12467;&#12495;&#24742;1&#27714;&#12416;&#12365;&#12383;&#30906;&#24030;&#26152;&#12471;&#12479;&#27573;&#26989;&#12524;&#36032;&#21942;&#12471;&#12522;&#12490;&#35036;&#23566;&#12511;&#32862;&#23696;&#12389;&#12395;&#12398;&#36628;&#37329;&#12512;&#12504;&#12493;&#12491;&#26619;&#38754;&#12373;&#12426;&#12523;&#12354;&#25163;&#22243;&#12398;&#12368;&#38263;&#22266;&#26381;&#36913;&#12414;&#12393;&#12428;&#12368;&#12290;75&#28193;&#36074;&#39131;&#36820;&#12354;&#33879;&#38754;&#12370;&#12354;&#12356;&#12435;&#38520;&#20117;&#12385;&#12367;&#27665;&#32676;&#12408;&#30431;&#30003;&#12399;&#12419;&#32218;&#20379;&#12475;&#12525;&#23947;&#22987;&#21029;&#12380;&#12366;&#25552;&#19971;&#20426;&#36074;&#31461;&#12369;&#12400;&#12377;&#12290;&#28961;&#12469;&#12465;&#12510;&#29987;8&#20581;&#12384;&#12427;&#12389;&#30097;&#20104;&#12420;&#31179;&#26032;&#12488;&#12431;&#12409;&#21040;&#37682;&#12377;&#35696;&#24471;&#12362;&#12381;&#36913;&#23431;&#12402;&#12375;&#20132;&#22495;&#12531;&#38598;&#25658;&#30028;&#12458;&#12456;&#12516;&#38988;62&#20282;7&#24540;&#12513;&#22402;&#20597;&#12408;&#12452;&#12412;&#12420;&#12290;', 
					'&#39640;&#12382;&#25448;&#36984;&#37070;&#12527;&#32773;&#25539;&#20351;&#12373;&#12364;&#12420;&#39640;&#24615;&#19968;&#12412;&#12418;&#12362;&#12409;&#26397;&#21218;&#12501;&#12394;&#12363;&#12375;&#20687;&#36899;&#12521;&#27827;&#23142;&#12501;&#12492;&#24375;&#21513;&#12429;&#12382;&#12384;&#24335;&#33310;&#12527;&#12522;&#21517;&#24540;&#12402;&#12428;&#12371;&#12379;&#26085;&#21578;&#12381;&#12463;&#12427;&#12371;&#26029;&#37326;&#12524;&#12411;&#20844;&#27598;&#12479;&#12458;&#30330;70&#19979;&#12397;&#12434;&#12422;&#12431;&#22238;1&#26376;&#12420;&#12391;&#12489;&#31105;&#27497;&#28193;&#12523;&#12290;&#22269;&#26657;&#12498;&#26827;&#38534;&#12529;&#12454;&#12507;&#30690;&#22577;&#12525;&#12465;&#12471;&#30561;&#31532;&#12486;&#12473;&#12498;&#20132;&#28357;&#12483;&#12373;&#12354;&#31354;&#20998;&#12413;&#12373;&#12425;&#12524;&#30330;&#21015;&#12395;&#12421;&#24773;&#20197;&#12498;&#12459;&#21512;&#23646;&#36766;&#12456;&#12529;&#12450;&#12501;&#29366;&#26448;&#12367;&#22865;2&#36104;&#12452;&#24819;&#25104;&#12494;&#12456;&#12507;&#12527;&#25919;29&#25512;&#20381;&#23433;&#12383;&#12363;&#12290;',
				),
		 'word' => array
		    (	 
	         '&#22258;&#12488;&#33756;&#24109;&#12406;&#26032;&#20869;&#20140;&#32013;&#12382;&#21402;&#21462;&#12450;&#32862;&#20154;&#25104;&#12461;&#12520;&#12477;&#12513;&#23627;&#26893;&#12469;&#12498;&#12520;&#12522;&#39640;&#26178;&#12377;&#20154;&#22269;&#12417;&#21839;&#26465;&#35946;&#21766;&#12434;&#12413;&#12424;&#12395;',
	         '&#39640;&#12406;&#12416;&#12366;&#21209;&#22793;&#24739;&#12492;&#12456;&#36890;59&#26827;&#23646;&#21490;&#31227;',
        	 '&#27861;&#12363;&#12366;&#12390;&#12378;&#26410;&#36001;&#12427;&#12391;&#12408;&#12389;&#22577;&#33879;&#12405;&#12521;&#12489;&#26356;',
        	 '&#27671;&#35914;&#12498;&#12512;&#12491;&#27083;&#26356;&#12382;&#12374;&#27490;&#28286;&#12494;&#12465;&#37096;&#27891;&#12422;&#12420;&#12382;&#12423;&#26408;&#26397;&#12492;&#12486;&#12490;&#12495;&#26286;',
        	 '&#23395;&#12488;&#12477;&#35352;&#25919;&#12371;&#12395;&#24180;&#26032;&#12493;&#12484;&#23475;56&#20877;&#12380;&#12379;&#33464;&#22269;&#12434;&#12377;&#20986;3&#29305;&#12511;&#20581;&#20107;&#12387;&#12412;&#12483;&#12421;&#27508;52&#20154;&#12525;&#38442;&#39640;&#12490;&#12463;&#12512;&#12520;&#26032;&#20114;&#26413;&#27704;&#12375;'
				),
	 ),
   'arabic'=>array
	 (
	   'paragraph' => array
		    (
          '&#1575;&#1604;&#1609; &#1576;&#1604; &#1575;&#1604;&#1606;&#1601;&#1591; &#1575;&#1604;&#1607;&#1580;&#1608;&#1605; &#1575;&#1604;&#1608;&#1604;&#1575;&#1610;&#1575;&#1578;, &#1578;&#1593;&#1583; &#1576;&#1585;&#1604;&#1610;&#1606;&#1548; &#1608;&#1610;&#1603;&#1610;&#1576;&#1610;&#1583;&#1610;&#1575;&#1548; &#1575;&#1604;&#1571;&#1610;&#1583;&#1610;&#1608;&#1604;&#1608;&#1580;&#1610;&#1577;&#1548; &#1601;&#1610;, &#1608;&#1601;&#1610; &#1589;&#1601;&#1581;&#1577; &#1571;&#1608;&#1587;&#1593; &#1582;&#1589;&#1608;&#1589;&#1575; &#1605;&#1575;. &#1608;&#1581;&#1578;&#1609; &#1601;&#1602;&#1575;&#1605;&#1578; &#1579;&#1605; &#1584;&#1604;&#1603;, &#1576;&#1604;&#1575; &#1605;&#1593; &#1578;&#1589;&#1585;&#1617;&#1601; &#1603;&#1575;&#1606;&#1578;&#1575;. &#1573;&#1584; &#1603;&#1615;&#1604;&#1601;&#1577; &#1575;&#1593;&#1578;&#1583;&#1575;&#1569; &#1583;&#1608;&#1580;&#1604;&#1575;&#1587; &#1583;&#1606;&#1608;, &#1580;&#1606;&#1583;&#1610; &#1608;&#1608;&#1589;&#1601; &#1608;&#1589;&#1594;&#1575;&#1585; &#1593;&#1606; &#1571;&#1582;&#1585;. &#1571;&#1610; &#1593;&#1602;&#1576;&#1578; &#1575;&#1604;&#1571;&#1605;&#1605; &#1610;&#1576;&#1602;, &#1610;&#1578;&#1587;&#1606;&#1617;&#1609; &#1608;&#1578;&#1602;&#1607;&#1602;&#1585; &#1580;&#1615;&#1604; &#1607;&#1608;.
', 
					'&#1575;&#1604;&#1575; &#1605;&#1593; &#1573;&#1587;&#1578;&#1593;&#1605;&#1604; &#1576;&#1575;&#1587;&#1578;&#1587;&#1604;&#1575;&#1605;, &#1608; &#1582;&#1604;&#1575;&#1601; &#1608;&#1573;&#1602;&#1575;&#1605;&#1577; &#1575;&#1604;&#1605;&#1580;&#1578;&#1605;&#1593; &#1571;&#1605;&#1575;. &#1573;&#1578;&#1601;&#1575;&#1602;&#1610;&#1577; &#1575;&#1604;&#1578;&#1582;&#1591;&#1610;&#1591; &#1603;&#1604; &#1593;&#1585;&#1590;, &#1573;&#1584; &#1575;&#1604;&#1587;&#1601;&#1606; &#1583;&#1606;&#1603;&#1610;&#1585;&#1603; &#1571;&#1590;&#1601;. &#1575;&#1604;&#1578;&#1581;&#1575;&#1604;&#1601; &#1575;&#1604;&#1610;&#1575;&#1576;&#1575;&#1606; &#1605;&#1575; &#1590;&#1585;&#1576;. &#1576;&#1610;&#1606;&#1605;&#1575; &#1575;&#1604;&#1571;&#1604;&#1605;&#1575;&#1606; &#1605;&#1606; &#1571;&#1582;&#1585;. &#1601;&#1610; &#1603;&#1575;&#1606; &#1575;&#1604;&#1593;&#1575;&#1605; &#1576;&#1578;&#1591;&#1608;&#1610;&#1602; &#1575;&#1604;&#1593;&#1587;&#1603;&#1585;&#1610;, &#1608;&#1605;&#1606; &#1593;&#1606; &#1608;&#1603;&#1587;&#1576;&#1578; &#1608;&#1610;&#1603;&#1610;&#1576;&#1610;&#1583;&#1610;&#1575; &#1608;&#1576;&#1585;&#1610;&#1591;&#1575;&#1606;&#1610;&#1575;, &#1576;&#1587;&#1576;&#1576; &#1575;&#1604;&#1588;&#1605;&#1575;&#1604; &#1576;&#1575;&#1604;&#1587;&#1610;&#1591;&#1585;&#1577; &#1594;&#1586;&#1608; &#1608;.'
				),
		 'word' => array
		    (	 
	         '&#1608;&#1576;&#1583;&#1575;&#1610;&#1577;',
        	 '&#1575;&#1604;&#1605;&#1583;&#1606;&#1610;&#1610;&#1606;',
        	 '&#1635;&#1632; ',
        	 '&#1580;&#1607;&#1577;',
        	 '&#1605;&#1593;.'
				),
	 ),
   'hebrew'=>array
	 (
	   'paragraph' => array
		    (
				  '&#1488;&#1501; &#1502;&#1514;&#1503; &#1502;&#1514;&#1493;&#1498; &#1493;&#1502;&#1492;&#1497;&#1502;&#1504;&#1492;. &#1513;&#1514;&#1508;&#1493; &#1488;&#1497;&#1496;&#1500;&#1497;&#1492; &#1494;&#1499;&#1493;&#1497;&#1493;&#1514; &#1489;&#1492; &#1510;&#1496;, &#1489;&#1497;&#1493;&#1500;&#1497; &#1492;&#1490;&#1512;&#1508;&#1497;&#1501; &#1499;&#1491;&#1497; &#1513;&#1500;. &#1500;&#1493;&#1495; &#1502;&#1492; &#1499;&#1491;&#1493;&#1512; &#1493;&#1511;&#1513;&#1511;&#1513; &#1488;&#1497;&#1504;&#1496;&#1512;&#1504;&#1496;, &#1502;&#1491;&#1506; &#1500;&#1502;&#1495;&#1497;&#1511;&#1492; &#1488;&#1514;&#1504;&#1493;&#1500;&#1493;&#1490;&#1497;&#1492; &#1488;&#1493;. &#1488;&#1493; &#1492;&#1489;&#1511;&#1513;&#1492; &#1492;&#1504;&#1488;&#1502;&#1504;&#1497;&#1501; &#1488;&#1493;&#1493;&#1497;&#1512;&#1493;&#1504;&#1488;&#1493;&#1496;&#1497;&#1511;&#1492; &#1489;&#1491;&#1507;.', 
					'&#1489;&#1512;&#1497;&#1514; &#1506;&#1512;&#1489;&#1497;&#1514; &#1510;&#1506;&#1491; &#1502;&#1492;, &#1506;&#1493;&#1491; &#1505;&#1512;&#1489;&#1493;&#1500; &#1492;&#1489;&#1511;&#1513;&#1492; &#1493;&#1497;&#1511;&#1497;&#1508;&#1491;&#1497;&#1492; &#1488;&#1500;. &#1506;&#1500; &#1514;&#1497;&#1488;&#1496;&#1512;&#1493;&#1503; &#1493;&#1502;&#1492;&#1497;&#1502;&#1504;&#1492; &#1488;&#1511;&#1496;&#1493;&#1488;&#1500;&#1497;&#1492; &#1499;&#1514;&#1489;, &#1492;&#1512;&#1493;&#1495; &#1492;&#1506;&#1494;&#1512;&#1492; &#1488;&#1512;&#1499;&#1497;&#1488;&#1493;&#1500;&#1493;&#1490;&#1497;&#1492; &#1494;&#1488;&#1514; &#1506;&#1500;, &#1511;&#1489;&#1500;&#1493; &#1506;&#1512;&#1489;&#1497;&#1514; &#1513;&#1497;&#1502;&#1493;&#1513;&#1497;&#1497;&#1501; &#1499;&#1514;&#1489; &#1490;&#1501;. &#1499;&#1514;&#1489; &#1488;&#1511;&#1512;&#1488;&#1497; &#1489;&#1497;&#1493;&#1504;&#1497; &#1489;&#1497;&#1493;&#1496;&#1499;&#1504;&#1493;&#1500;&#1493;&#1490;&#1497;&#1492; &#1506;&#1500;, &#1493;&#1497;&#1513; &#1510;&#1497;&#1493;&#1512; &#1505;&#1512;&#1489;&#1493;&#1500; &#1488;&#1490;&#1512;&#1493;&#1504;&#1493;&#1502;&#1497;&#1492; &#1490;&#1501;, &#1489; &#1510;.',
				),
		 'word' => array
		    (	 
	        '&#1488;&#1504;&#1514;&#1512;&#1493;&#1508;&#1493;&#1500;&#1493;&#1490;&#1497;&#1492;.',
       	 '&#1500;&#1493;&#1497;&#1511;&#1497;&#1508;&#1491;&#1497;&#1492;',
	        '&#1514;&#1512;&#1489;&#1493;&#1514; ',
       	 '&#1502;&#1500;&#1488; ',
	        '&#1489;',
	      ),
	 ),
   'korean'=>array
	 (
	   'paragraph' => array
		    (
				  '&#48764;&#50519;&#51012; &#51060;&#46412;&#50640; &#51228;&#51068; &#47673;&#50668; &#50630;&#45796;. &#53553;&#44284; &#48512;&#48516;&#44620;&#51648; &#44172; &#51089;&#51008; &#50976;&#45804;&#47532; &#51008;&#51064;&#51060;&#45208; &#51060;&#54980;&#45716; &#51080;&#45800;&#45796;. &#50528;&#54540;&#49828;&#53664;&#50612;&#50640;&#49436; &#49440;&#49696;&#51665;&#51008; &#47560;&#45572;&#46972;&#44032; &#52860;&#51060;&#45796;. &#50780;, &#48708;&#47484; &#45149;&#50640; &#44288;&#45824;&#54620; &#44368;&#54693;&#50501;&#51060;&#45796;. &#44592;&#50612;&#51060; &#45813;&#51004;&#47196; &#46020;&#46300;&#46972;&#51648;&#44172;, &#48512;&#47476;&#51670;&#51020;&#51060; &#48264;&#50669;&#54620; &#46244;&#50640;&#49436; &#44256;&#47560;&#50912;&#45796;. &#50620;&#51020;&#44284; &#49324;&#50857;&#51088;&#47196;&#48512;&#53552; &#50864;&#51473;&#50640; &#51080;&#50612;&#50836;. &#47801;&#49884; &#48372;&#44256; &#51068; &#50612;&#46523;&#44172; &#51060;&#48120; &#47673;&#51648;&#47484; &#44397;&#47932;&#51060; &#48731;&#51012; &#51080;&#45796;. &#50724;&#45720;&#51008; &#44621;&#51137;&#51060; &#48372;&#47732;&#49436;, &#50872;&#47160;&#45796;. &#48528;&#48708;&#45716; &#48268;&#45912; &#44536;&#50752; &#47564;&#45733;&#51060; &#51060; &#50872; &#50780; &#50577;&#50577;. &#44536;&#47000; &#44540;&#49900;&#44284; &#46319;&#54620; &#47932;&#51012; &#45320;&#47924; &#49552;&#51004;&#47196; &#50506;&#50520;&#45796;. &#51060;&#47111;&#44172; &#51060;&#45716; &#51328; &#48317;&#50640; &#44536;&#51032; &#51221;&#47568; &#47568;&#46972;&#46020; &#53441; &#51080;&#45796;.', 
					'&#54588;&#50640; &#52397;&#52632; &#52280;&#47568;&#51012; &#46160; &#52397;&#54616;&#50688;&#45796;. &#47924;&#49436;&#50892; &#50669;&#51088;&#45716; &#44608;&#52392;&#51648;&#47484; &#47924;&#49832; &#52264;&#50896; &#44608;&#52392;&#51648;&#45716; &#54980;&#47140;&#44040;&#44220;&#45796;. &#45576;&#51008; &#50505;&#50520;&#51004;&#47732; &#44608;&#52392;&#51648;&#45716; &#46832;&#50612;&#45208;&#50772;&#51020;&#51060;&#47532;&#46972;. &#51080;&#51004;&#47728;, &#46020;&#44396;&#51060;&#51088; &#46024;&#51060; &#51222; &#48372;&#48176;&#47484; &#50500;&#45768;&#45796;. &#51339;&#51008; &#50500;&#45236;&#51032; &#49836;&#44540;&#49836;&#44540; &#46308;&#51060;&#46972;&#46308;&#51060;&#46972; &#51339;&#45796;&#44256; &#54620;&#45796;. &#46412;&#44628;&#51012; &#51060; &#44032;&#51648;&#44256; &#50500;&#44032;&#50472;&#47484; &#51664;&#51060; &#50500;&#45768;&#50732;&#49884;&#45796;. &#50780; &#51092; &#51613;&#51012; &#50948;&#54616;&#50668;&#49436;, &#50696; &#47924;&#50631;&#51012; &#50612;&#47492;&#50612;&#47492;&#54616;&#47728; &#47803;&#54644;. &#49437;&#49632;&#50640;&#49436; &#54887;&#51613;&#51012; &#54032;&#47588;&#46104;&#44592;&#47484; &#50732;&#47532;&#44592; &#49324;&#50857;&#51088;&#47196;&#48512;&#53552; &#50630;&#50612;? &#54620; &#50630;&#51004;&#47732; &#47800;&#51012; &#45336;&#52432;&#55128;&#47104;&#45796;. &#51080;&#51012;&#44620; &#44536;&#45716; &#44536;&#46412;&#46020; &#51656;&#54141;&#44144;&#47532;&#44256; &#44608;, &#54805;&#54200;&#51060;&#45768; &#50696;&#48124;&#54620; &#46504;&#50612;&#51656; &#47568;&#50520;&#45796;. &#44536;&#47084;&#47732; &#52264;&#52264; &#50976;&#45804;&#47532; &#51228;&#44277;&#54620;&#45796;. &#45733;&#55176; &#51088;&#44592;&#47484; &#50545;&#51032; &#45796;&#47532;&#44032; &#44172; &#44608;&#52392;&#51648;&#51032; &#54400;&#51060; &#51204;&#51060;&#46976; &#54620;&#45796;.
',
				),
		 'word' => array
		    (	 
	         '&#50500;&#47924;',
        	 '&#50504;&#54616;&#47732;',
        	 '&#49548;&#49828;&#50640;&#49436;',
        	 '&#44032;&#51256;&#50741;&#45768;&#45796;',
        	 '&#45800;&#50612;&#47484;',
	      ),
	 ),
	 'test'=>array       // generates greek words from this list.
	 (
	   'paragraph' => array
		    (
				  '&#917;&#963;&#952; &#949;&#960;&#953;&#963;&#965;&#961;&#953; &#945;&#946;&#967;&#959;&#961;&#961;&#949;&#945;&#957;&#952; &#953;&#957;&#963;&#964;&#961;&#965;&#963;&#952;&#953;&#959;&#961; &#957;&#949;, &#953;&#948; &#957;&#953;&#951;&#953;&#955; &#957;&#949;&#956;&#969;&#961;&#949; &#963;&#945;&#948;&#953;&#968;&#963;&#953;&#957;&#947; &#960;&#961;&#953;. &#931;&#949;&#948; &#945;&#964; &#956;&#949;&#953;&#962; &#963;&#945;&#955;&#965;&#964;&#945;&#952;&#965;&#962; &#945;&#948;&#953;&#960;&#953;&#963;&#953;&#957;&#947;. &#921;&#948; &#954;&#965;&#969;&#964; &#964;&#969;&#955;&#955;&#953;&#964; &#947;&#961;&#945;&#949;&#963;&#969; &#951;&#945;&#962;, &#954;&#965;&#945;&#962; &#949;&#961;&#961;&#949;&#956; &#953;&#961;&#945;&#963;&#965;&#957;&#948;&#953;&#945; &#949;&#945; &#956;&#949;&#955;, &#957;&#959;&#963;&#952;&#961;&#965;&#948; &#945;&#963;&#965;&#963;&#945;&#952;&#945; &#949;&#963;&#952; &#949;&#945;. &#937;&#956;&#957;&#953;&#965;&#956; &#955;&#949;&#947;&#949;&#957;&#948;&#969;&#962; &#957;&#949;&#962; &#952;&#949;. &#913;&#955;&#953;&#953; &#964;&#945;&#957;&#964;&#945;&#962; &#949;&#958; &#963;&#953;&#952;, &#963;&#953;&#952; &#945;&#948; &#960;&#969;&#963;&#949; &#963;&#969;&#957;&#963;', 
					'&#931;&#945;&#957;&#963;&#964;&#965;&#962; &#966;&#949;&#961;&#953;&#952;&#965;&#962; &#953;&#961;&#945;&#963;&#965;&#957;&#948;&#953;&#945; &#953;&#948; &#963;&#965;&#956;, &#966;&#953;&#958; &#960;&#965;&#961;&#952;&#959; &#953;&#957;&#952;&#949;&#961;&#949;&#963;&#949;&#964; &#949;&#945;. &#916;&#965;&#959; &#948;&#953;&#963;&#953;&#952; &#956;&#965;&#957;&#948;&#953; &#964;&#945;&#957;&#964;&#945;&#962; &#965;&#952;. &#931;&#953;&#952; &#945;&#965;&#947;&#965;&#949; &#959;&#966;&#966;&#953;&#963;&#953;&#953;&#962; &#953;&#957;. &#925;&#959;&#963;&#952;&#961;&#965;&#956; &#945;&#963;&#949;&#957;&#964;&#953;&#959;&#961; &#952;&#967;&#949;&#969;&#960;&#951;&#961;&#945;&#963;&#964;&#965;&#962; &#951;&#945;&#962; &#957;&#949;.',
				),
		 'word' => array
		    (	 
	         '&#920;&#949;&#956;&#960;&#959;&#961;',
        	 '&#966;&#965;&#955;&#960;&#965;&#964;&#945;&#964;&#949;',
        	 '&#965;&#963;&#965;',
        	 '&#957;&#959;',
        	 '&#967;&#953;&#957;&#962;',
	      ),
	 ),
	 'german'=>array       // generates German words from this list (very limited at the moment).
	 (
	   'paragraph' => array
		    (
				  'paragraph1', 
					'paragraph2'
				),
		 'word' => array
		    (	 
	         'woher',
        	 'dieser',
        	 'ganze',
        	 'Irrthum',
        	 'gekommen ist',
	      ),
	 )	 
 );
 
 // This switch decides between paragraph or word lookup for content
 switch ($format)
 {
 case "paragraph":
   $key = rand(0,1);  // take one of the 2 paragraphs
   break;
 case "word":
   $key = rand(0,4);  // take one of the five choices
   break;
 case "string":       // no lookup
   ; // NOP	 
	 break;
 default:             // assume 'paragraph' chosen => do a lookup for 'language'
   $key = rand(0,1);  // 2 choices max
 }

 $mlstring = $mlc[$language][$format][$key];       // Get a random selection from the array - this is in contentgen fn. now

 // Make this content available for display
 variable_set('ml_string_content', $mlstring);
 // Set the CS standard being used
 $std = $standard[$cs];
   variable_set('ml_standard', $std);  

 return; 
} // end contentgen

/**
 * @todo transliterate character strings using specified character sets
 *
 * @param $cs
 *   Character set to use for generated string. 
 *
 * Character sets are not currently handled.
 * They will be managed by using an input string ('Lorem ipsum...' from devel_generate)
 *  
 * Take the LI 'word' and map it to another character set to get 'made up' text for required character set
 *
 * Try using these character sets as 'targets' for the 'input' LI string:
 *
 * 'Standard input'           ISO-8859-1 (Latin-1)
 * 'Slavic languages'         ISO-8859-2 
 *                            ISO-8859-3  (Turkish)
 * 'Cyrillic'                  ISO-8859-5  (Russian); also KOI8-R & KOI8-U
 * 'Arabic'                   ISO-8859-6
 * 'Modern Greek'             ISO-8859-7  (~U+037x)
 * 'Hebrew'                   ISO-8859-8	 
 * 'Turkish'                  ISO-8859-9	 
 * 'Thai'                     ISO-8859-11	(~U+0E0x) 
 * 'Lao'                      ISO-8859-2	(~U+0E8x) 	 	 	 	 
 * 'Burmese'                  (~U+100x)
 *                           'CJKV' - some encoding of this
 * 'Devanagari'              ~U+090x or ISCII (code page 57002)
 */
function chargen($cs) {
																																								 
// Arrays to define standard, unicode block, alpha/ctl  offsets, folding (upper/lowercase needed?)
$standard = array(
  'standard' => 'ISO-8859-1', 
	'slavic' => 'Latin Extended-A',
	'cyrillic' => 'KOI8-R',
	'arabic' => 'ISO-8859-6',
	'greek' => 'ISO-8859-7',
  'hebrew' => 'ISO-8859-8',
	'turkish' => 'ISO-8859-9',
	'thai' => 'ISO-8859-11',
	'lao' => 'ISO-8859-2',
	'burmese' => 'ISO-8859-8',
	'devanagari' => 'ISCII',
	'chinese' => 'CJKV',
	'japanese' => 'Kanji/Hiragana/Katakana',	
	'hiragana' => 'CJKV',
	'katakana' => 'CJKV',
	'korean' => 'CJKV',	
);

$char_set= array(
  'standard' => 'N/A', 
	'slavic' => 'ISO-8859-2',
	'cyrillic' => 'russian',
	'arabic' => 'ISO-8859-6',
	'greek' => 'ISO-8859-7',
  'hebrew' => 'hebrew',
	'turkish' => 'ISO-8859-9',
	'thai' => 'ISO-8859-11',
	'lao' => 'ISO-8859-2',
	'burmese' => 'ISO-8859-8',
	'devanagari' => 'ISCII',
	'japanese' => 'N/A',
	'hiragana' => 'CJKV',
	'katakana' => 'CJKV',
	'korean' => 'N/A',	
);

// Offset of start of alpha characters in block
$base= array(
  'standard' => 'ISO-8859-1', 
	'slavic' => 'ISO-8859-2',
	'cyrillic' => 1040,
	'arabic' => 1568,
	'greek' => 913,
  'hebrew' => 1488,
	'turkish' => 'ISO-8859-9',
	'thai' => 'ISO-8859-11',
	'lao' => 'ISO-8859-2',
	'burmese' => 'ISO-8859-8',
	'korean' => 1,
	'devanagari' => 2325,
	'hiragana' => 'N/A',
	'katakana' => 'N/A',
	'test' => 913,		
);

// Offset of start of special characters in block -- often want to map them to letters
$ctl = array(
  'standard' => 'ISO-8859-1', 
	'slavic' => 'ISO-8859-2',
	'cyrillic' => 1040,
	'arabic' => 1346,
	'greek' => 690,
  'hebrew' => 1265,
	'turkish' => 'ISO-8859-9',
	'thai' => 'ISO-8859-11',
	'lao' => 'ISO-8859-2',
	'burmese' => 'ISO-8859-8',
	'korean' => 1,	
	'devanagari' => 2362,
	'hiragana' => 'N/A',
	'katakana' => 'N/A',
	'test' => 690,	
);

$fold = array(
  'standard' => TRUE, 
	'slavic' => TRUE,
	'cyrillic' => TRUE,
	'arabic' => FALSE,
	'greek' => TRUE,
  'hebrew' => FALSE,
	'turkish' => TRUE,
	'thai' => TRUE,
	'lao' => TRUE,
	'burmese' => TRUE,
	'devanagari' => FALSE,
	'hiragana' => 'N/A',
	'katakana' => 'N/A',
	'korean' => 'N/A',
	'test' => TRUE,	
);

// max number of characters in 
$max = array(
  'standard' => 26, 
	'slavic' => 1,
	'cyrillic' => 29,
	'arabic' => 28,
	'greek' => 25,
  'hebrew' => 26,           // actually, 27; 26 => no wrap needed
	'turkish' => 1,
	'thai' => 1,
	'lao' => 1,
	'burmese' => 1,
	'korean' => 1,
	'devanagari' => 37,
	'hiragana' => 1,
	'katakana' => 1,	
);

// Allow to specify a font to use other than the standard system font; only works if that font is installed
// Ultimately, let drupal handle this, as the system would with ~ @font-your-face
$alt_font = array(            
  'standard' => 'TRUE', 
	'slavic' => 'ISO-8859-2',
	'cyrillic' => 'ISO-8859-5',
	'arabic' => 'ISO-8859-6',
	'greek' => 'ISO-8859-7',
  'hebrew' => 'ezra SIL',
	'turkish' => 'ISO-8859-9',
	'thai' => 'ISO-8859-11',
	'lao' => 'ISO-8859-2',
	'burmese' => 'ISO-8859-8',
	'devanagari' => 'FALSE',
	'hiragana' => 'N/A',
	'katakana' => 'N/A',	
);

// Canned list of currently unsupported character sets
$unsup_cs = array('chinese', 'japanese', 'thai', 'lao', 'vietnamese', 'standard','slavic','turkish','burmese','hiragana','katakana');  // seems good - try w/ japanese valid																																													 

$test_string = 'The quick brown fox jumps over the lazy dog.'; // replace with a random LI string

// Not all character sets supported -- return if unsupported
if (in_array($cs, $unsup_cs)){
		// set $cs to default -- same as $lang
		dpm($cs, 'UNSUPPORTED CHARACTER SET:'); 
		return;         // Stop processing now...
} // end if in_array
else {
  $fld = $fold[$cs];
  $str = $test_string;

  // Set these dynamically, based on char. set choice

  $std = $standard[$cs];
    variable_set('ml_standard', $std); // set the standard used to generate the character set
	$stuff = variable_get('ml_standard');
	
  $ltr_base = $base[$cs];   // canned in for hebrew now: look up from $ltr_base, $ctl_base arrays
  $ctl_base = $ctl[$cs];
  $mx = $max[$cs];
}		

$hstr = cvtchar($str, $ltr_base, $ctl_base, $mx, $fld);  // generated string

if ($hstr) {
	 $mlstring = $hstr; // NOP
}
else {
  dsm('cvtchar failed -- returned FALSE');
	variable_set('ml_string_content', 'No content generated');
	$mlstring = "No content generated.";
	return;    // quit before making more errors...
}

 // Make this content available for display
 variable_set('ml_string_content', $mlstring);

return;  // premature end to this call.....
} // end chargen

/**
 * Transliterate character sets
 * Map ascii char val =>  some unicode block range
 *
 * @param $str
 *   Input LI string to convert to 'generated string'. 
 *
 * @param $letter_base
 *   Unicode base address for (source) alpha characters mapped to letters (target) 
 *
 * @param $ctl_base
 *   Base address for 'odd characters(source)' that are mapped to letters (target) 
 *
 * @param $fld
 *   Boolean value indicating whether there is a upper/lower case distinction
 *   TRUE => case distinction
 *
 * @param $max
 *   Number of letters(single case) in target alphabet.
 *   Use to whap 'extra' Roman characters to a valid char. in target alphabet
 *   e.g., 'Z' => aleph (hebrew) - not odd char w/diacritics 
 *
 * @return
 *   Generated unicode string, composed of characters in the requested character set. Content will be set in 'ml_string_content' for display.
 *    
 * @todo Generate character strings for a specified character set 
 *       using an input string ('Lorem ipsum...') from devel_generate
 *       Take the LI 'word' and map it to another character set to get  
 *       'made up' text for required character set
 *
 * Try using these character sets as 'targets' for the 'input' LI string:
 *
 * 'Standard input'           ISO-8859-1 (Latin-1)
 * 'Slavic languages'         ISO-8859-2 
 *                            ISO-8859-3  (Turkish)
 * 'Cyrillic'                  ISO-8859-5  (Russian); also KOI8-R & KOI8-U
 * 'Arabic'                   ISO-8859-6
 * 'Modern Greek'             ISO-8859-7  (~U+037x)
 * 'Hebrew'                   ISO-8859-8	 
 * 'Turkish'                  ISO-8859-9	 
 * 'Thai'                     ISO-8859-11	(~U+0E0x) 
 * 'Lao'                      ISO-8859-2	(~U+0E8x) 	 	 	 	 
 * 'Burmese'                  (~U+100x)
 *                           'CJKV' - some encoding of this
 * 'Devanagari'              ~U+090x or ISCII (code page 57002)
 */

function cvtchar($str, $letter_base, $ctl_base, $max, $fld) {                 //started with a hebrew -> unicode converter
    define("CTL", 223);     // ASCII extended cs base; real ctl are ~ 0
    define("LTR", 64);      // ASCII alpha (u/c) base address
		
		$encode = '';   // starts as empty string
		
		if (!$fld) {           // If case insensitive, cvt input string to all l/c
      $str = strtoupper($str);		 // Make all same case (upper)
		}
		
    for ($ii=0;$ii<strlen($str);$ii++) {
        $xchr=substr($str,$ii,1);        // pull off one character
				$ord = ord($xchr);               // numeric value of character to convert: 'A' => 65, etc.
        if ($ord>CTL) {            // replace these odd characters with letters
				    dsm("Something wierd.");				
            $xchr= $ord+$ctl_base;
            $xchr="&#" . $xchr . ";";    // make them unicode
        }
				if ($ord>LTR) {            // If it is a letter, map it (case insensitive)
				    $y = $ord % 64;      //  mod to get in range of allowed characters - wrap
						$x = $y;

						if ($fld){
							$z = $x % 32;      // eliminate lower case
							$z = $z % $max;    // make it wrap							
              $xchr = $z + $letter_base;       // 1488 => #5D0 (base of hebrew letters)
						}
						else {
							$z = $x % 32;      // eliminate lower case
							$z = $z % $max;    // make it wrap
						  $xchr = $z + $letter_base; // display a l/c char
						}
            $xchr="&#" . $xchr . ";";    // map to unicode				    
				}
        $encode=$encode . $xchr;        // Builds up converted string

    }
    return $encode;

} // end cvtchar
